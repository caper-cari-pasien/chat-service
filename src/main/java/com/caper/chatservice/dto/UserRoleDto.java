package com.caper.chatservice.dto;

public enum UserRoleDto {
    PATIENT, DOCTOR
}
