package com.caper.chatservice.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TreatmentChatRequest {
    private String content;
    private Date time;
    private String treatmentId;
    private String senderId;
}
