package com.caper.chatservice.controller;

import java.util.List;
import org.springframework.http.ResponseEntity;
import lombok.RequiredArgsConstructor;
import com.caper.chatservice.dto.TreatmentChatDto;
import com.caper.chatservice.dto.TreatmentChatRequest;
import com.caper.chatservice.service.TreatmentChatService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/chat")
@RequiredArgsConstructor
public class ChatController {

    private final TreatmentChatService chatService;

    @PostMapping("")
    public ResponseEntity<TreatmentChatDto> sendTreatmentChat(@RequestBody TreatmentChatRequest treatmentChatRequest) {
        var response = chatService.sendTreatmentChat(treatmentChatRequest);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{treatmentId}")
    public ResponseEntity<List<TreatmentChatDto>> findByTreatmentId(@PathVariable("treatmentId") String treatmentId) {
        var response = chatService.findByTreatmentId(treatmentId);
        return ResponseEntity.ok(response);
    }


}
