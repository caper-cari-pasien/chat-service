package com.caper.chatservice.exceptions;

public class WebClientNotFoundException extends RuntimeException{
    public WebClientNotFoundException(String message){
        super(message);
    }
}
