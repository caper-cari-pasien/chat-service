package com.caper.chatservice.service;

import org.springframework.stereotype.Service;
import java.util.List;
import com.caper.chatservice.dto.TreatmentChatDto;
import com.caper.chatservice.dto.TreatmentChatRequest;
import com.caper.chatservice.repository.TreatmentChatRepository;
import lombok.RequiredArgsConstructor;


@Service
@RequiredArgsConstructor
public class TreatmentChatService {
    
    private final TreatmentChatRepository chatRepository;

    public TreatmentChatDto sendTreatmentChat(TreatmentChatRequest chatRequest) {
        return chatRepository.sendTreatmentChat(chatRequest);
    }

    public List<TreatmentChatDto> findByTreatmentId(String treatmentId) {
        return chatRepository.findByTreatmentId(treatmentId);
    }
}
