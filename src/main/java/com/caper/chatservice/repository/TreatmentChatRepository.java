package com.caper.chatservice.repository;
import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;
import com.caper.chatservice.dto.TreatmentChatDto;
import com.caper.chatservice.dto.TreatmentChatRequest;
import com.caper.chatservice.exceptions.WebClientNotFoundException;
import java.util.List;


@Repository
public class TreatmentChatRepository {
    private final WebClient client;

    public TreatmentChatRepository(@Value("${db-service-url}") String baseUrl) {
        client = WebClient.create(baseUrl + "/api/treatment/chat");
    }

    public List<TreatmentChatDto> findByTreatmentId(String treatmentId) {
        try {
            return client.get().uri("/" + treatmentId).retrieve().bodyToFlux(TreatmentChatDto.class).collectList().block();
        } catch (WebClientResponseException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                throw new WebClientNotFoundException("Invalid user or treatment id");
            }
            throw e;
        }
    }

    public TreatmentChatDto sendTreatmentChat(TreatmentChatRequest treatmentChatRequest) {
        try{
            return client.post().uri("").body(Mono.just(treatmentChatRequest), TreatmentChatRequest.class)
                    .retrieve().bodyToMono(TreatmentChatDto.class).block();
        }catch (WebClientResponseException e) {
            if(e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                throw new WebClientNotFoundException("Invalid user or treatment id");
            }
            throw e;
        }
    }
}
