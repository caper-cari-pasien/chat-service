package com.caper.chatservice;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@SpringBootTest
class ChatServiceApplicationTests {

    @Test
	void testApplicationStartsWithoutExceptions() {
        assertDoesNotThrow(() -> ChatServiceApplication.main(new String[]{}));
	}
}
