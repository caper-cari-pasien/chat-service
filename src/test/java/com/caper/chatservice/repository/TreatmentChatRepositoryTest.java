package com.caper.chatservice.repository;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.caper.chatservice.dto.TreatmentChatDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.junit.jupiter.api.extension.ExtendWith;
import java.util.List;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.*;
import static com.caper.chatservice.utils.TestUtils.*;
import com.caper.chatservice.exceptions.WebClientNotFoundException;


@ExtendWith(SpringExtension.class)
class TreatmentChatRepositoryTest {
    private static MockWebServer mockWebServer;
    private TreatmentChatRepository chatRepository;

    @BeforeEach
    void setUp() throws Exception {
        mockWebServer = new MockWebServer();
        mockWebServer.start();
        String baseUrl = String.format("http://localhost:%s", mockWebServer.getPort());
        chatRepository = new TreatmentChatRepository(baseUrl);
    }

    @AfterEach
    void tearDown() throws Exception {
        mockWebServer.shutdown();
    }

    @Test
    void testFindByTreatmentIdSuccess() throws Exception {
         // Set Up
        var treatmentId = "test treatmentId";
        var expectedChatDto = createTreatmentChatDto();
        mockWebServer.enqueue(new MockResponse()
                .setBody(new ObjectMapper().writeValueAsString(expectedChatDto))
                .addHeader("Content-Type", "application/json"));

        // Test
        List<TreatmentChatDto> actualChatDto = chatRepository.findByTreatmentId(treatmentId);

        // Assert
        assertNotNull(actualChatDto);
        assertEquals(1, actualChatDto.size());
        assertEquals(expectedChatDto, actualChatDto.get(0));
        assertEquals(expectedChatDto.getId(), actualChatDto.get(0).getId());
        assertEquals(expectedChatDto.getContent(), actualChatDto.get(0).getContent());
        assertEquals(expectedChatDto.getTime(), actualChatDto.get(0).getTime());
        assertEquals(expectedChatDto.getUser(), actualChatDto.get(0).getUser());
    }

    @Test
    void testFindByTreatmentIdThrowWebClientNotFoundException() {
        // Set Up
        var treatmentId = "test treatmentId";
        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(404)
                .setBody("{}")
                .addHeader("Content-Type", "application/json"));

        // Test and Assert
        assertThrows(WebClientNotFoundException.class, () -> chatRepository.findByTreatmentId(treatmentId));
    }

    @Test
    void testFindByTreatmentIdThrowException() {
        // Set up
        var treatmentId = "test treatmentId";
        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(500)
                .setBody("{}")
                .addHeader("Content-Type", "application/json"));

        // assert
        assertThrows(WebClientResponseException.class, () -> chatRepository.findByTreatmentId(treatmentId));
    }


    @Test
    void testSendTreatmentChatSuccess() throws Exception {
        // Set Up
        var chatRequest = createTreatmentChatRequest();
        var chatDto = createTreatmentChatDto();

        // Test
        mockWebServer.enqueue(new MockResponse()
                .setBody(new ObjectMapper().writeValueAsString(chatDto))
                .addHeader("Content-Type", "application/json"));

        // Act
        var result = chatRepository.sendTreatmentChat(chatRequest);

        // Assert
        assertNotNull(result);
        assertEquals(chatDto.getId(), result.getId());
        assertEquals(chatDto.getContent(), result.getContent());
        assertEquals(chatDto.getTime(), result.getTime());
        assertEquals(chatDto.getUser(), result.getUser());
    }

    @Test
    void testSendTreatmentChatThrowWebClientNotFoundException() {
        // Set Up
        var chatRequest = createTreatmentChatRequest();
        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(404)
                .setBody("{}")
                .addHeader("Content-Type", "application/json"));

        // Test and Assert
        assertThrows(WebClientNotFoundException.class, () -> chatRepository.sendTreatmentChat(chatRequest));
    }

    @Test
    void testSendTreatmentChatThrowException() {
        // Set up
        var chatRequest = createTreatmentChatRequest();
        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(500)
                .setBody("{}")
                .addHeader("Content-Type", "application/json"));

        // assert
        assertThrows(WebClientResponseException.class, () -> chatRepository.sendTreatmentChat(chatRequest));
    }

}
