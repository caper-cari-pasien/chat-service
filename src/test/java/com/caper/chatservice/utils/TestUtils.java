package com.caper.chatservice.utils;

import java.util.Date;
import com.caper.chatservice.dto.TreatmentChatDto;
import com.caper.chatservice.dto.TreatmentChatRequest;
import com.caper.chatservice.dto.UserDto;
import com.caper.chatservice.dto.UserRoleDto;


public class TestUtils {
    public static UserDto createUserDto() {
        return UserDto.builder().id("id user").nama("nama").username("username")
                .role(UserRoleDto.DOCTOR).umur(45).domisili("domisili").build();
    }

    public static TreatmentChatRequest createTreatmentChatRequest() {
        return TreatmentChatRequest.builder().content("content").time(new Date())
                .treatmentId("test treatmentId").senderId("test senderId").build();
    }

    public static TreatmentChatDto createTreatmentChatDto() {
        return TreatmentChatDto.builder().id("id chat").content("content").time(new Date())
        .user(createUserDto()).build();
    }

}

