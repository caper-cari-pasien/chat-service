package com.caper.chatservice.controller;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import com.caper.chatservice.dto.TreatmentChatDto;
import com.caper.chatservice.service.TreatmentChatService;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static com.caper.chatservice.utils.TestUtils.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class ChatControllerTest {

    @InjectMocks
    private ChatController chatController;

    @Mock
    private TreatmentChatService chatService;

    @Test
    void testSendTreatmentChatReturnsOkResponse() {
        var request = createTreatmentChatRequest();
        var response = createTreatmentChatDto();
        when(chatService.sendTreatmentChat(request)).thenReturn(response);

        // Test
        var result = chatController.sendTreatmentChat(request);

        // Assert
        assertNotNull(result);
        verify(chatService, times(1)).sendTreatmentChat(request);
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(response, result.getBody());
    }

    @Test
    void testFindByTreatmentIdReturnsOkResponse() {
        var treatmentId = "test treatmentId";
        List<TreatmentChatDto> response = new ArrayList<>();
        when(chatService.findByTreatmentId(treatmentId)).thenReturn(response);

        // Test
        var result = chatController.findByTreatmentId(treatmentId);

        // Assert
        assertNotNull(result);
        verify(chatService, times(1)).findByTreatmentId(treatmentId);
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(response, result.getBody());
    }

}
