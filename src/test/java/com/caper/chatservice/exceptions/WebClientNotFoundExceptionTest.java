package com.caper.chatservice.exceptions;

import org.junit.jupiter.api.Test;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
class WebClientNotFoundExceptionTest {

    @Test
    void testConstructorAndGetMessage() {
        String errorMessage = "WebClient not found";
        var exception = new WebClientNotFoundException(errorMessage);
        assertEquals(errorMessage, exception.getMessage());
    }
}
