package com.caper.chatservice.service;

import com.caper.chatservice.dto.TreatmentChatDto;
import com.caper.chatservice.dto.TreatmentChatRequest;
import com.caper.chatservice.repository.TreatmentChatRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static com.caper.chatservice.utils.TestUtils.*;

@ExtendWith(SpringExtension.class)
class TreatmentChatServiceTest {

    @InjectMocks
    private TreatmentChatService chatService;

    @Mock
    private TreatmentChatRepository chatRepository;

    @Test
    void testSendTreatmentChatReturnsChatDto() {
        // Set Up
        var request = createTreatmentChatRequest();
        var expectedResponse = createTreatmentChatDto();
        when(chatRepository.sendTreatmentChat(request)).thenReturn(expectedResponse);

        // Test
        var actualResponse = chatService.sendTreatmentChat(request);

        // Assert
        assertNotNull(actualResponse);
        assertEquals(expectedResponse.getId(), actualResponse.getId());
        assertEquals(expectedResponse.getContent(), actualResponse.getContent());
        assertEquals(expectedResponse.getTime(), actualResponse.getTime());
        assertEquals(expectedResponse.getUser(), actualResponse.getUser());
        verify(chatRepository, times(1)).sendTreatmentChat(any(TreatmentChatRequest.class));
    }

    @Test
    void testFindByTreatmentIdReturnsListOfChatDto() {
        // Set Up
        var treatmentId = "test treatmentId";
        var treatmentChatDto = createTreatmentChatDto();
        List<TreatmentChatDto> expectedResponse = new ArrayList<>();
        expectedResponse.add(treatmentChatDto);
        when(chatRepository.findByTreatmentId(treatmentId)).thenReturn(expectedResponse);

        // Test
        var actualResponse = chatService.findByTreatmentId(treatmentId);

        // Assert
        assertNotNull(actualResponse);
        assertEquals(expectedResponse.get(0).getId(), actualResponse.get(0).getId());
        assertEquals(expectedResponse.get(0).getTime(), actualResponse.get(0).getTime());
        assertEquals(expectedResponse.get(0).getContent(), actualResponse.get(0).getContent());
        assertEquals(expectedResponse.get(0).getUser(), actualResponse.get(0).getUser());
        verify(chatRepository, times(1)).findByTreatmentId(any(String.class));
    }
    
}
